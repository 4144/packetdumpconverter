#! /usr/bin/env python
# -*- coding: utf8 -*-
#
# Copyright (C) 2017  Andrei Karas
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

def showHelp():
    if len(sys.argv) != 2 or sys.argv[1] == "":
        print "Usage: ./tshark_to_php.py filename"
        exit(1)


def fileCompare(fileName):
    idx1 = fileName.rfind("_")
    idx2 = fileName.rfind(".txt")
    if idx1 < 0 or idx2 < 0:
        return fileName
    key = fileName[idx1 + 1 : idx2]
    key = ("0" * (5 - len(key))) + key
    return key


def getFiles(tshark_name):
    files = []
    for name in os.listdir("data/tshark"):
        if name.find(tshark_name) != 0:
            continue
        files.append(name)
    return sorted(files, key=fileCompare)


def skipFor(f, text):
    for line in f:
        if line == text:
            return True
    return False


def getPort(text):
    idx = text.find(":")
    if idx < 0:
        return 0
    return int(text[idx + 1 : ])


def saveData(w, line, isCS):
    if len(line) < 2:
        return
    if isCS == True:
        w.write("5353\n")
        w.write(line)
    else:
        w.write("5252\n")
        w.write(line)


def convertFile(name, w):
    print "Converting file: {0}".format(name)
    tabSC = True
    with open(name, "r") as f:
        ret = skipFor(f, "===================================================================\n")
        if ret == False:
            print "Cant find stream separator"
            return
        for line in f:
            if line == "Follow: tcp,raw\n":
                continue
            elif line.find("Filter: tcp.stream eq ") == 0:
                continue
            elif line.find("Node 0: ") == 0:
                port = getPort(line[8:])
                if port in (6900, 6901, 6121, 6122, 5121, 5122):
                    tabSC = True
                if line.find("192.168.") > 0:
                    tabSC = False
            elif line.find("Node 1: ") == 0:
                port = getPort(line[8:])
                if port in (6900, 6901, 6121, 6122, 5121, 5122):
                    tabSC = False
                if line.find("192.168.") > 0:
                    tabSC = True
            elif line == "===================================================================\n":
                break
            elif line == "" or line == "\n":
                continue
            elif line[0] == "\t":
                saveData(w, line[1:], tabSC)
            else:
                saveData(w, line, not tabSC)


def convertFiles(tshark_name, files):
    with open("data/php/" + tshark_name + ".txt", "w") as w:
        for name in files:
            convertFile("data/tshark/" + name, w)


showHelp();
tshark_name = sys.argv[1]
files = getFiles(tshark_name)
convertFiles(tshark_name, files)
