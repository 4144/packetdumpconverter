#!/bin/bash
# Copyright (C) 2017  Andrei Karas
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

if [ "$1" == "" ]; then
    echo "Usage: ./pcap_to_tshark.sh filenam"
    exit 1
fi

FILTER="tcp.port == 6901 || tcp.port == 6122 || tcp.port == 5122 || tcp.port == 6900 || tcp.port == 6950 || tcp.port == 6121 || tcp.port == 5121 || tcp.port == 4500 || (tcp.srcport > 10000 && tcp.srcport < 10100) || (tcp.dstport > 10000 && tcp.dstport < 10100)"

n=0

while true; do
    filename="data/tshark/${1}_$((n)).txt"
    rm "$filename" 2>/dev/null
    tshark -2 -z "follow,tcp,raw,$((n))" -r data/pcap/${1} -R "$FILTER" >${filename}

    if [ ! -f $filename ]; then
        break
    fi
    grep -P "\t" $filename >/dev/null

    if [ "$?" != 0 ]; then
        rm "$filename"
#        break
    fi

    echo "Dumped to data/pcap/${1} to $filename"
    n=$((n+1))

    if [[ $n -ge 100 ]]; then
        break
    fi
done
